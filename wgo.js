// Copyright (C) 2023 Jari Ronkainen, licenced under 3-Clause BSD Licence https://opensource.org/license/bsd-3-clause/
"use strict";

const CANVAS_OFFSCREEN = 0x1;

function load_shader(gl, type, source) {
    const shader = gl.createShader(type);

    gl.shaderSource(shader, source);
    gl.compileShader(shader);

    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
        alert(
            `Shader compilation error: ${gl.getShaderInfoLog(shader)}`
        );

        gl.deleteShader(shader);
        return null;
    }

    return shader;
}

function link_shader(gl, vs_src, frag_src) {
    const vertex_shader = load_shader(gl, gl.VERTEX_SHADER, vs_src);
    const fragment_shader = load_shader(gl, gl.FRAGMENT_SHADER, frag_src);

    const program = gl.createProgram();

    gl.attachShader(program, vertex_shader);
    gl.attachShader(program, fragment_shader);
    gl.linkProgram(program);

    if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
        alert(`Shader link error: ${gl.getProgramInfoLog(program)}`);
        return null;
    }

    return program;
}

class Shader {
    #context;
    #program;

    attributes = {};
    uniforms = {};

    static #current_shader;

    constructor(context, vertex_source, fragment_source, info) {
        Shader.#current_shader = null;

        this.#context = context;
        this.#program = link_shader(context, vertex_source, fragment_source);

        for (const attrib in info.attributes) {
            Object.defineProperty(this.attributes, attrib, {
                value: {
                    location: context.getAttribLocation(this.#program, attrib),
                    dimension: info.attributes[attrib].dimension,
                    type: info.attributes[attrib].type,
                    normalise: info.attributes[attrib].normalise,
                    stride: info.attributes[attrib].stride,
                    offset: info.attributes[attrib].offset,
                },
                writable: false,
                enumerable: true,
            });
        }

        for (const uniform in info.uniforms) {
            Object.defineProperty(this.uniforms, uniform, {
                value: context.getUniformLocation(this.#program, info.uniforms[uniform]),
                writable: false,
                enumerable: true,
            });
        }

    }

    use() {
        this.#setup_attributes()
        this.#context.useProgram(this.#program);
        Shader.#current_shader = this.#program;
    }

    #setup_attributes() {
        if (Shader.#current_shader !== null) {
            for (const attrib in Shader.#current_shader.attributes) {
                Shader.#context.disableVertexAttribArray(Shader.#current_shader.attributes[attrib].location);
            }
        }

        for (const attrib in this.attributes) {
            this.#context.vertexAttribPointer(
                this.attributes[attrib].location,
                this.attributes[attrib].dimension,
                this.attributes[attrib].type,
                this.attributes[attrib].normalise,
                this.attributes[attrib].stride,
                this.attributes[attrib].offset
            );

            this.#context.enableVertexAttribArray(this.attributes[attrib].location);
        }
    }
}

function create_quad(gl) {
//    const position_buffer = gl.createBuffer();
    const pos = [-1.0, 1.0, 0.0,
                  1.0, 1.0, 0.0,
                  1.0,-1.0, 0.0,
                 -1.0,-1.0, 0.0];

    /*
    gl.bindBuffer(gl.ARRAY_BUFFER, position_buffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(pos), gl.DYNAMIC_DRAW);

    return position_buffer;
    */

    return pos;
}

// TODO: allow 3D arrows?
function create_arrow(gl, [x0, y0], [x1, y1]) {
//    const position_buffer = gl.createBuffer();

    const vec_len = Math.sqrt((x1 - x0) ** 2, (y1 - y0) ** 2);
    const head_len = 7.0;
    const half_width = 6.0;

    const unit_vec_x = (x1 - x0) / vec_len;
    const unit_vec_y = (y1 - y0) / vec_len;
    const tangent_x = -unit_vec_y;
    const tangent_y = unit_vec_x;

    const head0_x = x1 - head_len * unit_vec_x + half_width * tangent_x;
    const head0_y = y1 - head_len * unit_vec_y + half_width * tangent_y;
    const head1_x = x1 - head_len * unit_vec_x - half_width * tangent_x;
    const head1_y = y1 - head_len * unit_vec_y - half_width * tangent_y;

    const pos = [x0,      y0,       0.0,
                 x1,      y1,       0.0,
                 x1,      y1,       0.0,
                 head0_x, head0_y,  0.0,
                 x1,      y1,       0.0,
                 head1_x, head1_y,  0.0];

    /*
    gl.bindBuffer(gl.ARRAY_BUFFER, position_buffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(pos), gl.DYNAMIC_DRAW);
    */

    return pos;
}

function canvas_to_display_size(canvas) {
    const actual_width = canvas.clientWidth;
    const actual_height = canvas.clientHeight;

    if (canvas.width  !== actual_width ||
        canvas.height !== actual_height) {

        canvas.width = actual_width;
        canvas.height = actual_height;
    }

    return [canvas.width, canvas.height];
}


function create_common_canvas_element() {
    const full_page_canvas = document.createElement("canvas");

    full_page_canvas.style.position = "absolute";
    full_page_canvas.style.top = "0";
    full_page_canvas.style.left = "0";
    full_page_canvas.style.width = "100%";
    full_page_canvas.style.height = "100vh";
    full_page_canvas.style.zIndex = "-1";
    full_page_canvas.style.display = "block";
    full_page_canvas.style.pointerEvents = "none";

    document.body.appendChild(full_page_canvas);

    return full_page_canvas;
}

function get_script_element() {
    let attempt = document.currentScript;
    if (attempt !== null)
        return attempt;

    const scripts = document.getElementsByTagName("script");

    // TODO: verify this
    for(let i = scripts.length - 1; i >= 0; --i) {
        const attempt = scripts[i];
        if (attempt.hasAttribute("src") === false)
            continue;

        return attempt;
    }

    return null;
}

function rewrite_div(element) {
    if (element.nodeType != Node.ELEMENT_NODE)
        return;

    const div = document.createElement('div');
    const set_width = -1;
    const set_height = -1;

    for (const index in element.attributes) {
        const attr = element.attributes[index];
        if (attr.name === "width") {
            div.style.width=attr.value + "px";
        }
        else if (attr.name === "height") {
            div.style.height=attr.value + "px";
        }
        else if (attr.value != undefined)
            div.setAttributeNS(null, attr.name, attr.value);
    }

    div.style.pointerEvents = "none";

    div.innerHTML = element.innerHTML;

    element.replaceWith(div);

    return div;
}

const shaders = {}

function create_grid_shader(context) {
    const vs_shader_source = `
        attribute vec3 in_vertex_pos;

        void main() {
            gl_Position = vec4(in_vertex_pos, 1.0);
        }
    `;

    const fs_shader_source = `
        precision mediump float;

        uniform vec2 cell_size;

        uniform mediump vec2 canvas_offset;

        void main() {
            vec4 grid_colour = vec4(0.4, 0.4, 0.4, 1.0);
            vec4 background_colour = vec4(1.0, 1.0, 1.0, 1.0);

            if (int(mod(gl_FragCoord.x - canvas_offset.x, cell_size.x)) == 0 ||
                int(mod(gl_FragCoord.y - canvas_offset.y, cell_size.y)) == 0) {
                gl_FragColor = vec4(grid_colour);
            } else {
                gl_FragColor = vec4(background_colour);
            }
        }
    `;

    const features = {
        attributes: {
            in_vertex_pos: {
                dimension: 3,
                type: context.FLOAT,
                normalise: false,
                stride: 0,
                offset: 0
            },
        },
        uniforms: {
            canvas_offset: "canvas_offset",
            cell_size: "cell_size",
        }
    }

    return new Shader(context, vs_shader_source, fs_shader_source, features)
}

function create_default_shader(context) {
    const vs_shader_source = `
        attribute vec3 in_vertex_pos;
//        attribute vec4 in_vertex_col;

//        varying vec4 out_vertex_col;

        uniform mediump vec2 canvas_size; 

        void main() {
            vec2 pos_xy = in_vertex_pos.xy * 2.0 / canvas_size.xy - vec2(1.0, 1.0);

//            out_vertex_col = in_vertex_col;
            gl_Position = vec4(pos_xy, 0.0, 1.0);
        }
    `;

    const fs_shader_source = `
        precision mediump float;

        uniform mediump vec2 canvas_offset;
//        varying vec4 out_vertex_col;

        void main() {
//            gl_FragColor = out_vertex_col;
            gl_FragColor = vec4(0.0, 0.0, 0.0, 1.0);
        }
    `;

    const features = {
        attributes: {
            in_vertex_pos: {
                dimension: 3,
                type: context.FLOAT,
                normalise: false,
                stride: 0,
                offset: 0
            },
            /*
            in_vertex_col: {
                dimension: 4,
                type: context.FLOAT,
                normalise: false,
                stride: 0,
                offset: 0
            }
            */
        },
        uniforms: {
            canvas_size: "canvas_size"
        }
    }

    return new Shader(context, vs_shader_source, fs_shader_source, features)
}

class GraphicsObject {
    shader = null;
    uniforms = {
        time_offset: 0.0,
        canvas_size: null,
        canvas_offset: null,
        bgcolour: [0.0, 0.0, 0.0, 0.0],
    };
    vertices = {
        count: 0,
        buffers: {}
    };
}

class Grid extends GraphicsObject
{
    constructor(context, features) {
        super();
        if (shaders[context] === null || shaders[context] === undefined) {
            shaders[context] = {};
        }

        if (shaders[context].grid_shader === undefined) {
            shaders[context].grid_shader = create_grid_shader(context);
        }

        this.shader = shaders[context].grid_shader;
        this.vertices = {
            primitive_type: context.TRIANGLE_FAN,
            count: 4,
            buffers: {
                in_vertex_pos: create_quad(context),
            }
        }

        this.uniforms.cell_size = features.cell_size;
    }
}

class Arrow extends GraphicsObject
{
    constructor(context, features) {
        super();
        if (shaders[context] === null || shaders[context] === undefined) {
            shaders[context] = {};
        }

        if (shaders[context].default_shader === undefined) {
            shaders[context].default_shader = create_default_shader(context);
        }

        this.shader = shaders[context].default_shader;
        this.vertices = {
            primitive_type: context.LINES,
            count: 6,
            buffers: {
                in_vertex_pos: create_arrow(context,
                                            features.begin,
                                            features.end),
            }
        }
    }
}

function is_suitable_queue(queue, object) {
    console.log(object);

    if (object.shader != queue.shader)
        return false;

    if (object.vertices.primitive_type != queue.vertices.primitive_type)
        return false;

    /*
     * TODO:
    if (attributes_match(object.vertices.buffers, queue.vertices.buffers)) {
    }

    if (uniforms_match)
    */

    return false;

//    return true;
}

function append_to_queues(queues, graphicsobject) {
    const len = Object.keys(queues).length;

    for (const queue_index in queues) {
        if (is_suitable_queue(queues[queue_index], graphicsobject)) {
            console.log("suitable queue found");
        }
    }

    const new_queue = {
        shader: graphicsobject.shader,
        uniforms: graphicsobject.uniforms,
        vertices: graphicsobject.vertices,
    }

    queues.push(new_queue);
}

function create_gl_buffers(context, queues) {
    queues.forEach((queue) => {
        for (const attrib in queue.vertices.buffers) {
            const vertices = queue.vertices.buffers[attrib];

            const buffer = context.createBuffer();
            context.bindBuffer(context.ARRAY_BUFFER, buffer);
            context.bufferData(context.ARRAY_BUFFER, new Float32Array(vertices), context.STATIC_DRAW);

            queue.vertices.buffers[attrib] = buffer;
        }

        console.log(queue);
    });
}

function parse_scene(context, description) {
    // TODO: sort queues by renderer -> similar uniforms -> etc.

    let queues = [];

    console.log("input description:", description);

    description.forEach(object => {
        const cmd = "new " + object.type + "(context, " + JSON.stringify(object) + ");";

        const graphicsobject = eval(cmd);
        append_to_queues(queues, graphicsobject);
    });

    create_gl_buffers(context, queues);

    console.log("final rendering queues:", queues);

    return queues;
}

function rewrite_canvases_to_divs() {
    const rewrite_queue = document.querySelectorAll('canvas.wgo-canvas');

    for (const index in rewrite_queue)
        rewrite_div(rewrite_queue[index])
}

class Canvas
{
    #owned_context;
    #context;
    #element;

    #observer;

    scene = {};
    queues = [];

    constructor(element, context = null) {
        if (element.nodeType != Node.ELEMENT_NODE) {
            alert("error 315");
        }

        if (context === null) {
            this.#element = element;
            this.#context = element.getContext("webgl", {
                preserveDrawingBuffer: true
            });
            this.#owned_context = true;
        } else {
            this.#element = rewrite_div(element);
            this.#context = context;
            this.#owned_context = false;
        }

        if (element.hasAttribute("data-scene-src")) {
            exit("unimplemented data-scene-src")
        }
        else {
            const json_element = element.querySelector("script");
            this.scene = JSON.parse(json_element.innerHTML)
        }

        this.recreate_queues();

        this.#observer = new ResizeObserver(entries => {
//            this.recreate_queues();
            this.render();
        });

        this.#observer.observe(this.#context.canvas);
    }

    recreate_queues() {
        this.queues = []
        this.queues = parse_scene(this.#context, this.scene);
    }

    setup_canvas() {
        const gl = this.#context;

        gl.clearColor(0.5, 1.0, 0.5, 1.0);
        gl.clearDepth(1.0);
        gl.enable(gl.DEPTH_TEST);

        gl.depthFunc(gl.LEQUAL);

        const canvas_size = canvas_to_display_size(gl.canvas);

        console.log(this.#element);
        if (this.#element.nodeName === "DIV") {
            // common context stuff
            gl.enable(gl.SCISSOR_TEST);
            const drawing_area = this.#element.getBoundingClientRect();
            if (drawing_area.bottom < 0 || drawing_area.top  > gl.canvas.clientHeight ||
                drawing_area.right < 0  || drawing_area.left > gl.canvas.clientWidth) {
                return CANVAS_OFFSCREEN;
            }
            gl.viewport(drawing_area.left,
                        gl.canvas.clientHeight - drawing_area.bottom,
                        drawing_area.right - drawing_area.left,
                        drawing_area.bottom - drawing_area.top);
            gl.scissor(drawing_area.left,
                       gl.canvas.clientHeight - drawing_area.bottom,
                       drawing_area.right - drawing_area.left,
                       drawing_area.bottom - drawing_area.top);
        } else {
            // owned context, no need for tricks
            gl.disable(gl.SCISSOR_TEST);
            gl.viewport(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight);
        }

        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    }

    setup_buffers(queue) {
        const gl = this.#context;

        for (const buffer in queue.shader.attributes) {
            console.log("bindBuffer: ", queue.vertices.buffers[buffer]);
            gl.bindBuffer(gl.ARRAY_BUFFER, queue.vertices.buffers[buffer]);
            console.log("setting attribute pointer", queue.shader.attributes[buffer].location);

            // TODO:
        }
    }

    setup_shaders(queue) {
        queue.shader.use();

        for (const uniform_index in queue.shader.uniforms) {
            if (uniform_index === "canvas_size") {
                if (this.#element.nodeName === "DIV") {
                    const drawing_area = this.#element.getBoundingClientRect();

                    const size = [
                        drawing_area.right - drawing_area.left,
                        drawing_area.bottom - drawing_area.top
                    ];

                    this.#context.uniform2fv(
                        queue.shader.uniforms[uniform_index],
                        size
                    );
                } else {
                    this.#context.uniform2fv(
                        queue.shader.uniforms[uniform_index],
                        [this.#context.canvas.clientHeight, this.#context.canvas.clientWidth]
                    );
                }
            } else if (uniform_index === "canvas_offset") {
                if (this.#element.nodeName === "DIV") {
                    const drawing_area = this.#element.getBoundingClientRect();

                    const offsets = [
                        drawing_area.left,
                        this.#context.canvas.clientHeight - drawing_area.bottom,
                    ];

                    this.#context.uniform2fv(
                        queue.shader.uniforms[uniform_index],
                        offsets
                    );
                } else {
                    this.#context.uniform2fv(
                        queue.shader.uniforms[uniform_index],
                        [0.0, 0.0]
                    );
                }
            } else {
                if (queue.uniforms[uniform_index] === undefined) {
//                    alert("undefined uniform");
                    continue;
                }

                if (queue.uniforms[uniform_index].length == 1) {
                    this.#context.uniform1fv(
                        queue.shader.uniforms[uniform_index],
                        queue.uniforms[uniform_index]
                    );
                }
                else if (queue.uniforms[uniform_index].length == 2) {
                    this.#context.uniform2fv(
                        queue.shader.uniforms[uniform_index],
                        queue.uniforms[uniform_index]
                    );
                }
                else if (queue.uniforms[uniform_index].length == 3) {
                    this.#context.uniform3fv(
                        queue.shader.uniforms[uniform_index],
                        queue.uniforms[uniform_index]
                    );
                }
                else if (queue.uniforms[uniform_index].length == 4) {
                    this.#context.uniform4fv(
                        queue.shader.uniforms[uniform_index],
                        queue.uniforms[uniform_index]
                    );
                }
                else {
                    alert("invalid uniform length");
                }
            }
        }
    }

    draw_queue(queue) {
        {
            this.#context.drawArrays(queue.vertices.primitive_type,
                                     0,
                                     queue.vertices.count);
        }
    }

    prepare_queue(queue) {
        this.setup_buffers(queue);
        this.setup_shaders(queue);
    }

    render() {
        if (this.setup_canvas() === CANVAS_OFFSCREEN)
            return;

        for (const queue_name in this.queues) {
            const queue = this.queues[queue_name];

            this.prepare_queue(queue);
            this.draw_queue(queue);
        }
    }
}

function create_canvases(common_context = null) {
    const targets = document.querySelectorAll('canvas.wgo-canvas');

    const canvas_list = []

    // TODO: sort canvases by context
    for (const index in targets) {
        const element = targets[index];
        if (element.nodeType != Node.ELEMENT_NODE)
            continue;

        canvas_list.push(new Canvas(element, common_context))
    }


    return canvas_list;
}

function main() {
    const this_script = get_script_element()
    console.log(this_script);
    const options = this_script.attributes["data-options"].value.split(' ');

    let context;
    let canvas;

    const glContextAttributes = { preserveDrawingBuffer: true };

    if (!options.includes("no-common-context")) {
        canvas = create_common_canvas_element();
        context = canvas.getContext("webgl", glContextAttributes);

        if (context === null) {
            exit("Unable to initialise webgl");
        }
    }

    const canvas_list = create_canvases(context);

    for (const canvas_index in canvas_list) {
        const canvas = canvas_list[canvas_index];
        canvas.render();
    }
}

main();

