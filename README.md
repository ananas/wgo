WebGL objects
=============

Bloody simple way to create webgl canvases with some content.


Usage
-----

Add to your html `<head>`
```
<script src="wgo.js" type="module" data-options="no-common-context" defer></script>
```

And then create canvases like

```
<canvas class="wgo-canvas" width="256" height="256">
    <script type="application/json">
        {
            "Grid": {
                "cell_size" : [17, 21]
            }
        }
    </script>
</canvas>
```

See `example.html`

### Trying it out with python

To try it out locally, the easiest way is to run a python http server
```
python -m http.server --bind localhost
```

And then checking out `localhost:8000` in your favourite browser



Extending
---------

### GraphicObjects


### Shaders

#### Uniforms automatically passed to shaders
If a shader is created with following uniform names, they are automagically passed
to the shader.

- `time_offset` doesn't do anything yet, but is reserved

- `canvas_offset` tells the offset from 0,0 the canvas actually starts.  Might be useful
  for some shaders.

- `canvas_size` is unsurprisingly the size of the rendering area.

